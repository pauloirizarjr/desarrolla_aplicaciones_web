**Desarrollo aplicaciones web con conexión a base de datos**
**Semestre:**
*5AVP*
**Alumnos:**
*Paulo Cesar Irizar Cazares*

- Practica #1 - 02/09/2022 - Práctica de Ejemplo
Commit: 968243bbcc95b689bd704a53fab43976e7adbd4a
Archivo: https://gitlab.com/pauloirizarjr/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Practica #2 - 09/09/2022 - Práctica de Ejemplo
Commit: 6a3d1f07724c61aa2c605d645dd44a2dcaf24e42
Archivo: https://gitlab.com/pauloirizarjr/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

- Practica #3 - 15/09/2022 - Práctica Web con Base de Datos
Commit: d1189c26b5ec79b25648856ca03cf1eda10e08bf

- Practica #4 - 19/09/2022 - Práctica Web con Base de Datos - Vista de Consula de Datos
Commit: ae39b5041309d89f2b73a30d6b594f2feaa1ad9c

- Práctica #5 - 22/09/2022 - Práctica Web con Base de Datos - Vista de Registro de Datos
Commit: ae39b5041309d89f2b73a30d6b594f2feaa1ad9c

- Práctica #6 - 26/09/2022 - Práctica Web con Base de Datos - Conexion de Base de Datos
Commit: f28f73014f5ea35f2002f1a333f39b109876e3b8
